(function (name, global, factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery", "ramda"], function ($, _) {
            return (global[name] = factory($, _, window, document));
        });
    } else if (typeof module === "object" && module.exports) {
        module.exports = (global[name] = factory(require("jquery"), require("ramda"), window, document));
    } else {
        global[name] = factory(jQuery, R, window, document);
    }
}("accordion", this, function ($, _, window, document) {
    var plugin = function (element, options) {
        var $el = $(element),
            opts = $.extend({}, true, plugin.defaults, options),
            $headers = $el.find(opts.sel.header),
            $bodies = $el.find(opts.sel.body),

            getClientWidth = function () {
                return window.innerWidth || document.documentElement.clientWidth;
            },

            largestFirst = _.comparator(function (a, b) {
                return a.width > b.width;
            }),

            shouldBeEnabled = _.curry(function (breakpoints, clientWidth) {
                if (breakpoints.length) {
                    var gte = _.compose(_.gte(clientWidth), _.prop("width"));
                    var sorted = _.sort(largestFirst, breakpoints);
                    var breakpoint = _.find(gte, sorted);
                    return breakpoint && breakpoint.on;
                }
                return true;
            }),

            setEnabled = function () {
                if (shouldBeEnabled(opts.breakpoints, getClientWidth())) {
                    if (!accordion.isEnabled()) {
                        accordion.enable();
                    }
                } else  {
                    accordion.disable();
                }
                setTimeout(setEnabled, opts.timeout);
            },

            close = function (header, body) {
                header.removeClass(opts.css.open);
                body.removeClass(opts.css.open);
                body.slideUp(opts.speed);
            },

            open = function (header, body) {
                body.slideDown(opts.speed);
                body.addClass(opts.css.open);
                header.addClass(opts.css.open);
            },

            isOpen = function (i, el) {
                return $(el).hasClass(opts.css.open);
            },

            filterOpen = _.filter(isOpen),

            handleToggle = function (source, target) {
                if (source.hasClass(opts.css.open)) {
                    close(source, target);
                } else {
                    if (opts.unique) {
                        close(filterOpen($headers), filterOpen($bodies));
                    }

                    open(source, target);
                }
            },

            accordion = {
                options: function () { return opts; },
                $element: function () { return $el; },

                isEnabled: function () {
                    return $el.hasClass(opts.css.enabled);
                },

                enable: function () {
                    $el.addClass(opts.css.enabled);
                    $el.on(opts.events.click + " " + opts.events.keydown, function (event) {
                        if (event.type === "click" || event.type === "keydown" && event.keyCode === opts.keys.enter) { 
                            accordion.toggle(event);
                        }
                    });
                    $headers.attr("tabindex", 0);
                    $bodies.slideUp(0);
                },

                disable: function () {
                    $el.removeClass(opts.css.enabled);
                    $el.off(opts.events.click);
                    $el.off(opts.events.keydown);
                    $headers.removeAttr("tabindex");
                    $bodies.slideDown(0);
                },

                toggle: function (event) {
                    var source = $(event.target),
                        sel = source.data(opts.data.target),
                        target = $el.find(sel);

                    if (sel && target.length) {
                        handleToggle(source, target);
                    }
                }
            };

        if (shouldBeEnabled(opts.breakpoints, getClientWidth())) {
            accordion.enable(); 
        }

        if (opts.breakpoints.length) {
            setTimeout(setEnabled, opts.timeout);
        }

        return accordion;
    };

    plugin.defaults = {
        unique: true,
        speed: 500,
        timeout: 1000,
        keys: {
            enter: 13
        },
        css: {
            enabled: "accordion-enabled",
            open: "accordion-open"
        },
        sel: {
            header: ".accordion-header",
            body: ".accordion-body"
        },
        data: {
            name: "name",
            target: "target"
        },
        events: {
            click: "click.accordion",
            keydown: "keydown.accordion"
        },
        breakpoints: [
            // { width: 480, on: true },
            // { width: 768, on: false },
            // { width: 992, on: true },
            // { width: 1200, on: false }
        ]
    };

    $.fn.accordion = function (options) {
        return this.each(function () {
            $.data(this, "accordion", plugin(this, options));
        });
    };

    return plugin;
}));
