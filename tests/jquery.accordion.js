(function (global, q, $, _, sinon) {

	var fixture = $(".accordion").accordion({
			speed: 0,
			breakpoints: [
				{ width: 480, on: false },
				{ width: 768, on: true }
			]
		}),
		data = fixture.data("accordion"),
		toggle = sinon.spy(data, "toggle"),
		clickEvent = $.Event("click"),
		options = data.options();

	q.test("basic setup", function (assert) {
		assert.ok(options, "options should be set");
		assert.ok(fixture.hasClass(options.css.enabled), "should have enabled CSS class");

		fixture.trigger(clickEvent);

		assert.ok(toggle.withArgs(clickEvent).calledOnce, "should react to click events");
		assert.equal(fixture.find(options.sel.header).attr("tabindex"), 0, "tabindex should be set");
		assert.ok(fixture.find(options.sel.body).is(":hidden"), "body should start as hidden");
	});

	q.test("click header", function (assert) {
		var header = fixture.find(options.sel.header).first(),
			body = fixture.find(header.data(options.data.target));

		header.trigger($.Event("click"));
		assert.ok(body.hasClass(options.css.open), "should be visible after first click");

		header.trigger($.Event("click"));
		assert.ok(!body.hasClass(options.css.open), "should not be visible after second click");
	});
}(typeof window !== "undefined" ? window : this, QUnit, jQuery, R, sinon));
